#pragma once

class Utils
{
public:
	static bool Detour32(char* src, char* dst, const intptr_t len);
	static char* TrampHook32(char* src, char* dst, const intptr_t len);
	static int GetSpotifyVersion();
	static std::string HexString(BYTE* data, int len);
	static void RemoveForbiddenChar(std::string* str);
	static void RemoveForbiddenCharW(std::wstring* str);
	static std::wstring Utf8ToUtf16(const std::string& str);
	static void DownloadSong(std::string fileId, std::string fileUri, std::string key, std::string authToken);
	static std::string DownloadSpotifyUrl(std::string host, std::string path, std::string authToken);
	static std::string DownloadUrlOld(std::string host, std::string path, std::string authToken);
	static bool BadPtr(void* ptr);
};

